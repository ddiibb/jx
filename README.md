### Dream 开发的微信小程序版 编辑器

![logo](配置编辑器/static/gh_16e4613251a6_258.jpg)

### PC Web版

https://qq1719248506.github.io/配置编辑器/

## jar包缝合教程：暴力缝合，非代码修改。

准备工作：MT管理器或者NP管理器（Android 版）
，B包要用的包，A包要导出的包。

开始：A包中merge重命名，展开重命名的merge，选中需要导出的文件，导出。打开B包，导入刚才导出的重命名的merge文件，保存退出。B包就是缝合好的包。

多试几次，关键是找个好用的MT或者NP。不是你不会，是因为你的MT或着NP没那个功能。

1、打开需要导出的包：

![1](https://qq1719248506.github.io/png/182570473-ffbec49e-99de-417a-8b3c-5a2c1f8eff88.png)

2、给他重命名：

![2](https://qq1719248506.github.io/png/182570594-fb517091-32a1-482b-b0b1-f27b22e8866e.png)

3、选中，并导出你需要的文件：

![3](https://qq1719248506.github.io/png/182570767-a235339c-dbf6-4ce2-8602-67ea06d1c5c3.png)

4、随便取个名字，自己能找到就行，理解就好。

![4](https://qq1719248506.github.io/png/182571018-264c1410-a124-43c3-af71-00aea1c7dce9.png)

5、打开你最终要用的包，导入刚才那个包导出的东西。

![5](https://qq1719248506.github.io/png/182571381-e4c6f45b-2a75-4fee-bf77-190b0c64c73a.png)

6、导入完成后的样子：

![6](https://qq1719248506.github.io/png/182571468-3f15da3f-ac7b-45b0-9e37-c4959692d24d.png)
![7](https://qq1719248506.github.io/png/182571486-1d78730b-0698-46cb-9af9-a4128eee8cd7.png)

TVBox各路大佬配置

唐三：https://hutool.ml/tang

FongMi：https://ghproxy.com/raw.githubusercontent.com/FongMi/CatVodSpider/main/json/config.json

巧技：http://pandown.pro/tvbox/tvbox.json

俊于：http://home.jundie.top:81/top98.json

霜辉月明py：https://ghproxy.com/raw.githubusercontent.com/lm317379829/PyramidStore/pyramid/py.json

小雅js：http://101.34.67.237/config/2

菜妮丝：https://tvbox.cainisi.cf

Yoursmile7：https://agit.ai/Yoursmile7/TVBox/raw/branch/master/XC.json

熊猫是肥猫：http://我不是.肥猫.love:63/接口禁止贩卖

神器：https://神器每日推送.tk/pz.json

饭太硬：http://饭太硬.ga/tv

## 壁纸：

https://tian.chuqiuyu.tk

https://yun.chuqiuyu.tk

https://shuping.chuqiuyu.tk

https://jianbian.chuqiuyu.tk

https://bing.img.run/rand.php

http://www.kf666888.cn/api/tvbox/img

https://picsum.photos/1280/720/?blur=10

http://刚刚.live/图

http://饭太硬.ga/深色壁纸/api.php

https://tuapi.eees.cc/api.php?category=fengjing&type=302

##香雅情仓库

https://framagit.org/lzk23559/tvbox

##奇奇的网站

http://bbs.qiqiv.cn/thread-11973-1-1.html

## 增加软件内置源，改软件图标，替换系统菜单图标和修改软件名等操作

#### 如果要替换文件，
 - MT管理器中打开安装包，在res文件夹内找到  
 - 如图标 原包里的图标名5j.png 需先改名，如 “5j1.png”，然后在导入相同名的图标名5j.png。
 - 桌面图标：5j.png   设置菜单：7A.png  直播菜单：E1.png搜索菜单：NR.png历史菜单：Yu.png 背景图片：o8.png  

#### 内置链接方法：
 - MT管理器中打开安装包，找到 classes.dex文件，打开方式 “Dex编辑器++”
 - 发起新搜索 查找内容 HomeActivity
 - 找到 com.github.tvbox.osc.ui.activity 目录下的 HomeActivity 文件
 - 打开 HomeActivity 文件，右上方按钮点开搜索 const-string v5,""
 - 内置的源地址输入到" "引号中就完成了内置源。按 Esc 保存并退出，最后点安装包 “功能” 签名

 123 | 456
 ---------|---------
![box](https://agit.ai/wzs1973/test/raw/branch/master/img/mt01.jpg) | ![Pluto](https://agit.ai/wzs1973/test/raw/branch/master/img/mt04.jpg)
![box](https://agit.ai/wzs1973/test/raw/branch/master/img/mt02.jpg) | ![Pluto](https://agit.ai/wzs1973/test/raw/branch/master/img/mt05.jpg)
![box](https://agit.ai/wzs1973/test/raw/branch/master/img/mt03.jpg) | ![Pluto](https://agit.ai/wzs1973/test/raw/branch/master/img/mt06.jpg)

---------
### 分享他人的教学，实操可成。

DD大神的私人订制设置影视仓内外置源的图文教程

https://qun.qq.com/qqweb/qunpro/share?_wv=3&_wwv=128&appChannel=share&inviteCode=1YI1421j715&contentID=1gcG4g&businessType=2&from=181174&shareSource=5&biz=ka
